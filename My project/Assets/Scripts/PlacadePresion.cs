using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacadePresion : MonoBehaviour
{
    public GameObject Objetoplaca;
    public GameObject ColliderEscalera;

    void OnTriggerEnter(Collider other)
    {
        ColliderEscalera.gameObject.SetActive(true);
        Destroy(Objetoplaca);
    }
}

